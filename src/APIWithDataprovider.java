import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URLDecoder;

import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class APIWithDataprovider {
	

   @Test(dataProvider="dataProviderMethodclass", dataProviderClass=dataproviderClass.class)
	public void sendPost(String query, String encodedresponse) throws ClientProtocolException, IOException {
		  HttpClient client = new DefaultHttpClient();
		  HttpPost post = new HttpPost("http://keppeldev.affle.com/api/conversation");
		  JSONObject json = new JSONObject();
		  json.put("contexts", false);
		  json.put("query", query);
		  System.out.println(query);
		  StringEntity input = new StringEntity( json.toString());
		  input.setContentType("application/json");
		  //StringEntity input = new StringEntity("product");
		  post.setEntity(input);
		  post.setHeader(HttpHeaders.AUTHORIZATION, "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MzY5LCJmbmFtZSI6IiIsImxuYW1lIjpudWxsLCJlbWFpbCI6IiIsInBhc3N3b3JkIjpudWxsLCJwaG9uZSI6IiIsInNlY29uZGFyeV9waG9uZSI6bnVsbCwicGhvdG91cmwiOm51bGwsInNlcnZpY2VfcHJvdmlkZXIiOiJndWVzdCIsIm1ldGFkYXRhIjpudWxsLCJjdXJyZW50X2NoYW5uZWwiOm51bGwsImNvdW50cnlfbG9jYWxlIjoiY01zUlZtMGZnSVZ4bEdOYmFZUEZIbVhSbWk4MyIsImNoYW5uZWxfaWQiOm51bGwsImxhc3RfZW5xdWlyZWRfcHJvcGVydHkiOm51bGwsImxvZ291dF90aW1lIjpudWxsLCJpc19hY3RpdmUiOjEsImNyZWF0ZWRfb24iOiIyMDIwLTAxLTA4VDEyOjM0OjI3LjAyNVoiLCJtb2RpZmllZF9vbiI6bnVsbCwidmlzaXRvcl9jb3VudCI6MCwiaWF0IjoxNTc4NDg2ODY3fQ.EM1La1rcBt8M44-gsVlSxe67YYarLcUJNsmLE0N1PBs");
		  HttpResponse response = client.execute(post);
		  BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
		  String line = "";
		  String message = new String();
		  while ((line = rd.readLine()) != null) {
		
		   System.out.println(line);
		   message += line;
		  }
			String decodedUrl = URLDecoder.decode(encodedresponse, "UTF-8");

			System.out.println(decodedUrl);
			System.out.println("Asserting");
		  Assert.assertEquals(message, decodedUrl);
		  
		  System.out.println("Hello");
		   System.out.println(message);


	}
	}
