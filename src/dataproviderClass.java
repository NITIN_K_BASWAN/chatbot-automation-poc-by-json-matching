
import org.testng.annotations.DataProvider;

public class dataproviderClass {
	
    @DataProvider
	public static Object[][] dataProviderMethodclass() {
    	
    	Object[][] data = new Object[10][2];
    	data[0][0] ="What type of property is Corals at Keppel Bay ?";
    	data[0][1] ="%7B%22type%22%3A%22chip%22%2C%22meta%22%3A%7B%22grey_border%22%3Afalse%2C%22dotted%22%3Afalse%2C%22red_border%22%3Afalse%2C%22red_color%22%3Afalse%2C%22white_text%22%3Afalse%2C%22black_text%22%3Afalse%2C%22red_bg%22%3Afalse%2C%22single_line_chip%22%3Afalse%7D%2C%22data%22%3A%5B%7B%22type%22%3A%22query%22%2C%22data%22%3A%7B%22label%22%3A%22Amenities%22%2C%22value%22%3A%22what%20are%20the%20nearby%20amenities%20of%20Corals%20at%20Keppel%20Bay%3F%22%2C%22icon%22%3A%22https%3A%2F%2Fstorage.googleapis.com%2Fkeppel-land-images%2Ficons%2Fwooden-house.svg%22%7D%7D%2C%7B%22type%22%3A%22query%22%2C%22data%22%3A%7B%22label%22%3A%22View%20on%20Map%22%2C%22value%22%3A%22location%20of%20Corals%20at%20Keppel%20Bay%22%2C%22icon%22%3A%22https%3A%2F%2Fstorage.googleapis.com%2Fkeppel-land-images%2Ficons%2Flocation.svg%22%7D%7D%5D%2C%22msg%22%3A%22Corals%20at%20Keppel%20Bay%20is%20a%20condominium.%20Would%20you%20like%20to%20find%20out%20more%20about%20this%20residential%20development%3F%22%2C%22suggestion%22%3A%5B%5D%7D";
    	
       	data[1][0] ="Where is the actual location of the project site/development at Corals at Keppel Bay? ";
    	data[1][1] ="%7B%22type%22%3A%22map%22%2C%22meta%22%3A%7B%7D%2C%22data%22%3A%7B%22iframe_url%22%3A%22https%3A%2F%2Fwww.google.com%2Fmaps%2Fembed%3Fpb%3D!1m18!1m12!1m3!1d3988.8444660499654!2d103.81477331473911!3d1.2659509990766644!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31da1a91262f3501%253A0xb151c97fab8210d5!2sCorals%2Bat%2BKeppel%2BBay!5e0!3m2!1sen!2ssg!4v1552977378330%22%2C%22web_url%22%3A%22https%3A%2F%2Fwww.google.com%2Fmaps%2Fplace%2FCorals%2Bat%2BKeppel%2BBay%2F%401.265951%2C103.816962%2C15z%2Fdata%3D!4m5!3m4!1s0x0%3A0xb151c97fab8210d5!8m2!3d1.265951!4d103.816962%3Fhl%3Den%22%7D%2C%22msg%22%3A%22Corals%20at%20Keppel%20Bay%20is%20located%20at%20Keppel%20Bay%20Drive.%20%5CnYou%20can%20click%20on%20the%20Google%20map%20below%20to%20get%20directions%20to%20Corals%20at%20Keppel%20Bay.%22%2C%22suggestion%22%3A%5B%7B%22type%22%3A%22chip%22%2C%22data%22%3A%5B%7B%22type%22%3A%22query%22%2C%22data%22%3A%7B%22label%22%3A%22Facilities%22%2C%22value%22%3A%22Facilities%20of%20Corals%20at%20Keppel%20Bay%22%2C%22icon%22%3A%22https%3A%2F%2Fstorage.googleapis.com%2Fkeppel-land-images%2Ficons%2Fwooden-house.svg%22%7D%7D%5D%7D%5D%7D";

    	data[2][0] ="How to reach Corals at Keppel Bay";
    	data[2][1] ="%7B%22type%22%3A%22link%22%2C%22meta%22%3A%7B%7D%2C%22data%22%3A%7B%22label%22%3A%22Navigate%22%2C%22value%22%3A%22https%3A%2F%2Fmaps.google.com%2F%3Fsaddr%3DCurrent%2BLocation%26daddr%3DCorals%2BAt%2BKeppel%2BBay%22%2C%22icon%22%3A%22https%3A%2F%2Fstorage.googleapis.com%2Fkeppel-land-images%2Ficons%2Flocation.svg%22%7D%2C%22msg%22%3A%22Sure!%20Let%20me%20help%20you%20with%20the%20directions.%20Corals%20at%20Keppel%20Bay%20is%20located%20at%20Keppel%20Bay%20Drive.%22%2C%22suggestion%22%3A%5B%5D%7D";
    	
    	data[3][0] ="How many units are there in Corals at Keppel Bay ?";
    	data[3][1] ="%7B%22type%22%3A%22text%22%2C%22meta%22%3A%7B%7D%2C%22data%22%3A%22%22%2C%22msg%22%3A%22We%20have%20a%20total%20of%20366%20units%20at%20Corals%20at%20Keppel%20Bay.%22%2C%22suggestion%22%3A%5B%5D%7D";
    	
    	data[4][0] ="What types of units are available in Corals at Keppel Bay?";
    	data[4][1] ="%7B%22type%22%3A%22text%22%2C%22meta%22%3A%7B%7D%2C%22data%22%3A%22%22%2C%22msg%22%3A%22Corals%20at%20Keppel%20Bay%20offers%20luxury%20homes%20ranging%20from%201-%20to%204-bedroom%20apartments%20and%20penthouses.%22%2C%22suggestion%22%3A%5B%5D%7D";
    	
    	data[5][0] ="What is the size/area of 1/2/3/4-bedroom/brm/br unit/apartment/apt or penthouse in Corals at Keppel Bay?";
    	data[5][1] ="%7B%22type%22%3A%22list%22%2C%22meta%22%3A%7B%7D%2C%22data%22%3A%7B%22heading%22%3A%22%22%2C%22items%22%3A%5B%221BR%20-%20From%20570%20to%20700%20sqft%22%2C%222BR%20-%20from%20840%20to%201%2C012%20sqft%22%2C%223BR%20Deluxe%20-%201%2C399%20to%201%2C561%20sqft%22%2C%224BR%20-%20from%202%2C573%20to%202%2C777%20sqft%22%2C%224BR%20Deluxe%20-%20from%202%2C960%20to%203%2C444%22%2C%22Penthouse%20unit%20-%20from%204%2C725%20to%204%2C887%20sqft.%22%5D%2C%22description%22%3A%22The%20size%20of%20our%20units%20are%20as%20follows%3A%22%7D%2C%22msg%22%3A%22%22%2C%22suggestion%22%3A%5B%5D%7D";
    	
    	data[6][0] ="How many floors are there in Corals at Keppel Bay?";
    	data[6][1] ="%7B%22type%22%3A%22text%22%2C%22meta%22%3A%7B%7D%2C%22data%22%3A%22%22%2C%22msg%22%3A%22Corals%20at%20Keppel%20Bay%20has%2011%20low-%20to%20medium-rise%20blocks%20from%205%20to%2010%20storeys%20high.%22%2C%22suggestion%22%3A%5B%5D%7D";
    	
    	data[7][0] ="Can i get a copy of brochure at Corals at Keppel Bay?";
    	data[7][1] ="%7B%22type%22%3A%22link%22%2C%22meta%22%3A%7B%7D%2C%22data%22%3A%7B%22label%22%3A%22Brochure%22%2C%22value%22%3A%22https%3A%2F%2Fwww.keppellandlive.com%2Fcontent%2Fdam%2Fkeppel-land-live%2FProperty%2Fcorals%2FCorals-at-Keppel-Bay-Brochure.pdf%22%2C%22icon%22%3A%22https%3A%2F%2Fstorage.googleapis.com%2Fkeppel-land-images%2Ficons%2Fvisibility-button.svg%22%7D%2C%22msg%22%3A%22Sure!%20Here%20is%20a%20PDF%20version%20of%20our%20brochure.%22%2C%22suggestion%22%3A%5B%5D%7D";
    	
    	data[8][0] ="I want to get the virtual tour of Corals at Keppel Bay ";
    	data[8][1] ="%7B%22type%22%3A%22chip%22%2C%22meta%22%3A%7B%22grey_border%22%3Atrue%2C%22dotted%22%3Afalse%2C%22red_border%22%3Afalse%2C%22red_color%22%3Atrue%2C%22white_text%22%3Afalse%2C%22black_text%22%3Afalse%2C%22red_bg%22%3Afalse%2C%22single_line_chip%22%3Afalse%7D%2C%22data%22%3A%5B%7B%22type%22%3A%22query%22%2C%22data%22%3A%7B%22label%22%3A%221%20BR%20(Type%20a1b)%22%2C%22value%22%3A%221%20BR%20virtual%20tour%20of%20Corals%22%2C%22icon%22%3Afalse%7D%7D%2C%7B%22type%22%3A%22query%22%2C%22data%22%3A%7B%22label%22%3A%222%20BR%20(Type%20b1j%20mirror)%22%2C%22value%22%3A%222%20BR%20virtual%20tour%20of%20Corals%22%2C%22icon%22%3Afalse%7D%7D%5D%2C%22msg%22%3A%22I%20would%20love%20to%20show%20you%20a%20virtual%20tour%20of%20our%20apartments%20in%20Corals%20at%20Keppel%20Bay.%20Which%20unit%20would%20you%20like%20to%20see%3F%22%2C%22suggestion%22%3A%5B%5D%7D";
    	
    	data[9][0] ="May I have some pictures of the unit at corals at keppel bay before making a trip down?";
    	data[9][1] ="%7B%22type%22%3A%22chip%22%2C%22meta%22%3A%7B%22grey_border%22%3Afalse%2C%22dotted%22%3Afalse%2C%22red_border%22%3Afalse%2C%22red_color%22%3Afalse%2C%22white_text%22%3Afalse%2C%22black_text%22%3Afalse%2C%22red_bg%22%3Afalse%2C%22single_line_chip%22%3Afalse%7D%2C%22data%22%3A%5B%7B%22type%22%3A%22query%22%2C%22data%22%3A%7B%22label%22%3A%22Virtual%20Tour%22%2C%22value%22%3A%22show%20me%20virtual%20tour%20of%20Corals%20at%20Keppel%20Bay%22%2C%22icon%22%3A%22https%3A%2F%2Fstorage.googleapis.com%2Fkeppel-land-images%2Ficons%2Fvirtual_tour.png%22%7D%7D%2C%7B%22type%22%3A%22link%22%2C%22data%22%3A%7B%22label%22%3A%22Go%20to%20website%22%2C%22value%22%3A%22https%3A%2F%2Fkeppellandlive.com%2Fresidential%2Fsingapore%2Fcorals-at-keppel-bay.html%22%2C%22icon%22%3A%22https%3A%2F%2Fstorage.googleapis.com%2Fkeppel-land-images%2Ficons%2Fwebsite.svg%22%7D%7D%5D%2C%22msg%22%3A%22Sure!%20Simply%20let%20me%20know%20if%20you%20prefer%20to%20take%20a%20virtual%20tour%20of%20our%20apartments%20or%20check%20out%20our%20photo%20gallery.%5Cn%20Please%20choose%20from%20the%20option%20below%3A%22%2C%22suggestion%22%3A%5B%5D%7D";
 	  	
        return data;
    }


}
